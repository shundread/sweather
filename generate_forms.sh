#!/usr/bin/env sh

echo on

echo pyside-rcc resources.qrc -o lib/forms/resources_rc.py
pyside-rcc resources.qrc -o lib/forms/resources_rc.py

cd "design"
for file in *.ui
do
    target="${file%.ui}.py"
    echo pyside-uic "design/$file" -o "lib/forms/$target"
    pyside-uic $file -o "../lib/forms/$target"
done
