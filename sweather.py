#!/usr/bin/env python

################################################################################
# Sweather - Shundread's Weather                                               #
# Copyright (C) 2013  Thiago Chaves de Oliveira Horta <shundread@gmail.com>    #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################


# Default libs
import sys

# Qt imports
from PySide import QtGui
from PySide import QtCore

from lib.mainwindow import MainWindow
from lib import datafetcher

def main():
    application = QtGui.QApplication(sys.argv)
    application.setQuitOnLastWindowClosed(False)
    ################
    # Set app info #
    ################
    QtCore.QCoreApplication.setOrganizationName("Shundread")
    QtCore.QCoreApplication.setApplicationName("Sweather")

    mainWindow = MainWindow()
    mainWindow.loadSettings()
    mainWindow.show()
    result = application.exec_()
    datafetcher.listener.kill()
    sys.exit(result)

if __name__=="__main__":
    main()
else:
    print("Notice: This module is intended to be a standalone program.")

