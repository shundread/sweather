################################################################################
# Sweather - Shundread's Weather                                               #
# Copyright (C) 2013  Thiago Chaves de Oliveira Horta <shundread@gmail.com>    #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import threading
import urllib2

class __DataFetcher(threading.Thread):
    '''A thread class dedicated to performing web requests and delivering the
    response data to callback functions. Handles a queue of requests and
    performs them sequencially to play nice and easy with the online service.'''
    def __init__(self):
        threading.Thread.__init__(self)
        self.__queue = []
        self.__alive = False
        self.__lock = threading.Lock()
        self.__event = threading.Event()
        self.start()

    def request(self, url, callback, *extra_arguments, **kargs):
        url = url.encode("utf-8")
        with self.__lock:
            # Sign the request if this request requires a key
            if 'key' in kargs:
                url = "{0}&APPID={1}".format(url, kargs['key'])

            # Make sure we don't add duplicate requests to the queue. This is a
            # bit of a redundant check, since within the program this class was
            # written for there is another check for that, but we want to make
            # sure we're playing nice with the online service.
            for request in self.__queue:
                if url == request[0]:
                    print("{0} was already on the queue. Skipping.".format(url))
                    return

            print("Adding request: {0}".format(url))
            self.__queue.append((url, callback, extra_arguments))
            self.__event.set()

    def kill(self):
        '''Kills all unfinished requests.'''
        with self.__lock:
            print("Killing thread")
            self.__queue = []
            self.__alive = False
            self.__event.set()

    def __handle_queue(self):
        '''Deals with every request waiting in the queue.'''
        while True:
            ##########################################
            # Check the queue for unhandled requests #
            ##########################################
            with self.__lock:
                # Go to sleep if there's nothing waiting in the queue
                if len(self.__queue) == 0:
                    print("The request queue is empty.")
                    return
                (url, callback, extra_arguments) = self.__queue.pop(0)

            ###################################
            # Handle a request from the queue #
            ###################################
            print("Fetching data from {0}".format(url))
            response = urllib2.urlopen(url)
            data = response.read()

            # In the unlikely (but possible?) event that the thread was killed
            # before we could call the callback just skip it.
            with self.__lock:
                if self.__alive:
                    callback(data, *extra_arguments)
                else:
                    print("Thread was killed. Skipping callback.")
                    return

    def run(self):
        with self.__lock:
            self.__alive = True
        while True:
            self.__event.wait()
            self.__event.clear()
            self.__handle_queue()
            with self.__lock:
                if self.__alive == False:
                    return

################
# The instance #
################
listener = __DataFetcher()

