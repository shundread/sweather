# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'about.ui'
#
# Created: Wed Oct  9 08:40:08 2013
#      by: pyside-uic 0.2.13 running on PySide 1.1.1
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.setWindowModality(QtCore.Qt.NonModal)
        Dialog.resize(600, 320)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Dialog.sizePolicy().hasHeightForWidth())
        Dialog.setSizePolicy(sizePolicy)
        Dialog.setMinimumSize(QtCore.QSize(600, 320))
        Dialog.setMaximumSize(QtCore.QSize(600, 320))
        Dialog.setStyleSheet("background: rgb(0, 0, 0);\n"
"color: rgb(255, 255, 255);")
        Dialog.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        Dialog.setSizeGripEnabled(False)
        Dialog.setModal(True)
        self.verticalLayout = QtGui.QVBoxLayout(Dialog)
        self.verticalLayout.setSpacing(9)
        self.verticalLayout.setContentsMargins(6, 6, 6, 6)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.labelIcon = QtGui.QLabel(Dialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.labelIcon.sizePolicy().hasHeightForWidth())
        self.labelIcon.setSizePolicy(sizePolicy)
        self.labelIcon.setText("")
        self.labelIcon.setPixmap(QtGui.QPixmap(":/icons/logo"))
        self.labelIcon.setScaledContents(False)
        self.labelIcon.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.labelIcon.setObjectName("labelIcon")
        self.horizontalLayout.addWidget(self.labelIcon)
        self.labelText = QtGui.QLabel(Dialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.labelText.sizePolicy().hasHeightForWidth())
        self.labelText.setSizePolicy(sizePolicy)
        self.labelText.setStyleSheet("font-family: monospace")
        self.labelText.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        self.labelText.setWordWrap(True)
        self.labelText.setObjectName("labelText")
        self.horizontalLayout.addWidget(self.labelText)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.buttonBox = QtGui.QDialogButtonBox(Dialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.buttonBox.sizePolicy().hasHeightForWidth())
        self.buttonBox.setSizePolicy(sizePolicy)
        self.buttonBox.setStyleSheet("background: rgb(255, 255, 255);\n"
"color: rgb(0, 0, 0);")
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(Dialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("accepted()"), Dialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("rejected()"), Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtGui.QApplication.translate("Dialog", "About Sweather", None, QtGui.QApplication.UnicodeUTF8))
        self.labelText.setText(QtGui.QApplication.translate("Dialog", "<html><head/><body><p>~ $ sweather --about</p><p>Sweather 0.1 (Shundread\'s weather)</p><p>Sweather is written using <a href=\"https://qt-project.org/wiki/Category:LanguageBindings::PySide\"><span style=\" text-decoration: underline; color:#6666ff;\">PySide</span></a> for its user interface and <a href=\"http://openweathermap.org/api\"><span style=\" text-decoration: underline; color:#6666ff;\">Open Weather Map</span></a> for its weather forecasting capabilities. Some of the icons used in the program are borrowed from the <a href=\"http://tango.freedesktop.org/\"><span style=\" text-decoration: underline; color:#6666ff;\">Tango Desktop Project</span></a>. A lot of thanks is given those three projects for allowing me to develop such kind of software with so little effort.</p><p>This software is licensed non-exclusively for your use under the <a href=\"https://www.gnu.org/licenses/gpl.html\"><span style=\" text-decoration: underline; color:#6666ff;\">GPL v3</span></a>. Feel free to modify it and share it, but keep it open-source and don\'t attribute your own modifications of the software to me.</p><p>Thiago Chaves de Oliveira Horta (<a href=\"mailto:shundread@gmail.com\"><span style=\" text-decoration: underline; color:#6666ff;\">shundread@gmail.com</span></a>)</p><p>~ $</p></body></html>", None, QtGui.QApplication.UnicodeUTF8))

import resources_rc
