################################################################################
# Sweather - Shundread's Weather                                               #
# Copyright (C) 2013  Thiago Chaves de Oliveira Horta <shundread@gmail.com>    #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

#Stdlib imports
import urllib2
import json

# Qt Imports
from PySide import QtGui
from PySide import QtCore

# UI import
from forms.addlocation import Ui_Dialog

# Other imports
#from locationinfo import LocationInfo
from listlocationmodel import ListLocationModel


######################
# List item delegate #
######################
class ItemLocationDelegate(QtGui.QAbstractItemDelegate):
    ###################################
    #   #           margin        #   #
    #   ###########################   #
    # m # Country # margin # City # m #
    #   ###########################   #
    #   #           margin        #   #
    ###################################

    Margin = 10
    indexSelected = QtCore.Signal(QtCore.QModelIndex)

    def __init__(self, parent=None):
        super(ItemLocationDelegate, self).__init__(parent)
        self.currentRow = -1

    #########################
    # Calculating size hint #
    #########################
    def sizeHint(self, option, index):
        return QtCore.QSize(
            option.rect.width(),
            option.fontMetrics.height() + 2*ItemLocationDelegate.Margin
        )

    ####################
    # Drawing the item #
    ####################
    def paint(self, painter, option, index):
        if not index.isValid():
            return

        margin = ItemLocationDelegate.Margin
        colSize = int((option.rect.width() - 3*margin) / 2.0)
        rowSize = option.fontMetrics.height()

        # Choose the right pallete
        if index.row() == self.currentRow:
            painter.save()
            painter.setBrush(option.palette.highlightedText())
            painter.fillRect(option.rect, option.palette.highlight())
            painter.setPen(painter.brush().color())
        else:
            painter.save()
            painter.setBrush(option.palette.text())
            painter.setPen(painter.brush().color())

        # Drawing the city name
        city_rect = QtCore.QRect(
            option.rect.x() + margin, option.rect.y() + margin,
            colSize, rowSize
        )
        city = index.data(ListLocationModel.RoleCity)
        painter.drawText(
            city_rect,
            QtCore.Qt.AlignLeft | QtCore.Qt.AlignBottom,
            option.fontMetrics.elidedText(city, QtCore.Qt.ElideRight, colSize)
        )

        #Drawing the country name
        country_rect = QtCore.QRect(
            option.rect.x() + colSize + margin*2,
            option.rect.y() + margin,
            colSize,
            rowSize
        )
        country = index.data(ListLocationModel.RoleCountry)
        painter.drawText(
            country_rect,
            QtCore.Qt.AlignLeft | QtCore.Qt.AlignBottom,
            option.fontMetrics.elidedText(country, QtCore.Qt.ElideRight, colSize)
        )

        painter.restore()

        # Drawing a delimiter
        if index.row() == 0:
            return
        painter.save()
        painter.setPen(QtCore.Qt.gray)
        painter.drawLine(
            option.rect.x(),
            option.rect.y(),
            option.rect.x() + option.rect.width(),
            option.rect.y()
        )
        painter.restore()

    ##################
    # Handling input #
    ##################
    def editorEvent(self, event, model, option, index):
        if event.type() == QtCore.QEvent.MouseButtonPress:
            self.currentRow = index.row()
            self.indexSelected.emit(index)
        return True

#######################
# Add Location dialog #
#######################
class AddLocationDialog(QtGui.QDialog):
    def __init__(self, parent=None):
        #Set up UI
        super(AddLocationDialog, self).__init__(parent)
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

        ############################
        # Setting up the list view #
        ############################
        self.listLocation = ListLocationModel()
        self.itemLocationDelegate = ItemLocationDelegate(self)
        self.ui.viewLocation.setModel(self.listLocation)
        self.ui.viewLocation.setItemDelegate(self.itemLocationDelegate)
        self.itemLocationDelegate.indexSelected.connect(self.ui.viewLocation.setCurrentIndex)
        
        #Initialize the Ok button
        self.checkValidInput()

    # Properties
    @property
    def validInput(self):
        return self.ui.viewLocation.currentIndex().isValid()

    @property
    def currentLocation(self):
        if not self.ui.viewLocation.currentIndex().isValid():
            return None
        return self.listLocation.locations[self.ui.viewLocation.currentIndex().row()]

    # Enabling/Disabling the Ok button
    def checkValidInput(self):
        '''Toggles the value of the Ok Button according to whether the Dialog's
        data is valid.'''
        okButton = self.ui.buttonBox.button(QtGui.QDialogButtonBox.Ok)
        okButton.setEnabled(self.validInput)

    def onLocationSelected(self):
        self.checkValidInput()

    # Enabling/Disabling the search button
    def onCityEdit(self, city_name):
        self.ui.refreshButton.setEnabled(len(city_name) > 0)

    ################################
    # Refreshing the location list #
    ################################
    def onRefresh(self):
        city = self.ui.editCity.text().strip()
        country = self.ui.editCountry.text().strip()

        # Check for proper input
        if len(city) == 0:
            return

        # Fetch location candidates
        self.listLocation.grabSearchResults(
            self.listLocation.replaceDataWithJsonData,
            city,
            country
        )

        # Update the model, the view and delegate
        self.ui.viewLocation.clearSelection()
        self.itemLocationDelegate.currentRow = -1
        self.checkValidInput()


