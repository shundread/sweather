################################################################################
# Sweather - Shundread's Weather                                               #
# Copyright (C) 2013  Thiago Chaves de Oliveira Horta <shundread@gmail.com>    #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

'''This file offers the LocationInfo class, which represents a location and its
weather information in the Sweather program. Among other things, it is able to
fetch weather data from the internet.'''

# stdlib imports
import time
import urllib2
import json

#####################
# Utility functions #
#####################
def _indent(level):
    return "    " * level

class LocationInfo(object):
    UrlSearch   = u"http://api.openweathermap.org/data/2.5/find?q={0}&type=like&mode=json"
    UrlFromId   = u"http://api.openweathermap.org/data/2.5/weather?id={0}&mode=json"

    UnitsMetric   = u"&units=metric"
    UnitsImperial = u"&units=imperial"
    UrlUnits = UnitsMetric

    ExpirationTime = 1000*60*60 # 1 hour

    def __init__(self, json_data):
        '''Typically, you won't be instancing the objects yourself, but rather
        using a class method to obtain an instance or list of instances.'''
        self.json_data = json_data
        self.timeStamp = time.time()

    ##############
    # Properties #
    ##############
    @property
    def id(self): return self.json_data["id"]

    @property
    def city(self): return self.json_data["name"]

    @property
    def country(self): return self.json_data["sys"]["country"]

    @property
    def humidity(self):
        if "main" not in self.json_data: return None
        if "humidity" not in self.json_data["main"]: return None
        return self.json_data["main"]["humidity"]

    @property
    def latitude(self):
        if "coord" not in self.json_data: return None
        if "lat" not in self.json_data["coord"]: return None
        return self.json_data["coord"]["lat"]

    @property
    def longitude(self):
        if "coord" not in self.json_data: return None
        if "lat" not in self.json_data["coord"]: return None
        return self.json_data["coord"]["lon"]

    @property
    def pressure(self):
        if "main" not in self.json_data: return None
        if "pressure" not in self.json_data["main"]: return None
        return self.json_data["main"]["pressure"]
    
    @property
    def temperature(self):
        if "main" not in self.json_data: return None
        if "temp" not in self.json_data["main"]: return None
        return self.json_data["main"]["temp"]

    @property
    def temperatureMax(self):
        if "main" not in self.json_data: return None
        if "temp_max" not in self.json_data["main"]: return None
        return self.json_data["main"]["temp_max"]

    @property
    def temperatureMin(self):
        if "main" not in self.json_data: return None
        if "temp_min" not in self.json_data["main"]: return None
        return self.json_data["main"]["temp_min"]


    @property
    def temperatureUnit(self):
        if self.__class__.UrlUnits == self.__class__.UnitsMetric:
            return u'\xb0C'
        return "F"

    @property
    def weatherDesc(self):
        if "weather" not in self.json_data: return None
        if "description" not in self.json_data["weather"][-1]: return None
        return self.json_data["weather"][-1]["description"]

    @property
    def weatherIcon(self):
        if "weather" not in self.json_data: return None
        if "icon" not in self.json_data["weather"][-1]: return None
        return self.json_data["weather"][-1]["icon"]

    @property
    def weatherId(self):
        if "weather" not in self.json_data: return None
        if "id" not in self.json_data["weather"][-1]: return None
        return self.json_data["weather"][-1]["id"]

    @property
    def weatherMain(self):
        if "weather" not in self.json_data: return None
        if "main" not in self.json_data["weather"][-1]: return None
        return self.json_data["weather"][-1]["main"]

    @property
    def windDegrees(self):
        if "wind" not in self.json_data: return None
        if "deg" not in self.json_data["wind"]: return None
        return self.json_data["wind"]["deg"]

    @property
    def windSpeed(self):
        if "wind" not in self.json_data: return None
        if "speed" not in self.json_data["wind"]: return None
        return self.json_data["wind"]["speed"]

    @property
    def isOld(self):
        return time.time() - self.timeStamp > LocationInfo.ExpirationTime

    ##########################################
    # Converting to easily-readable raw data #
    ##########################################
    def raw_data(self):
        '''Returns a human-readable summary of a LocationInfo object's raw data'''
        return self.__raw_data("JsonData", self.json_data)

    def __raw_data(self, key, value, level=0):
        # Base case, our current value is not an iterable
        if not hasattr(value, "__iter__"):
            if key == "":
                return _indent(level) + repr(value) + ",\n"
            return _indent(level) + repr(key) + " : " + repr(value) + ",\n"

        # Second case, our current value is a list:
        if isinstance(value, list):
            if key == "":
                result = _indent(level) + "[\n"
            else:
                result = _indent(level) + repr(key) + " : [\n"
            for i, v in enumerate(value):
                result += self.__raw_data("", v, level+1)
            result += _indent(level) + "],\n"
            return result

        # Last case, our current value is a dict
        if key == "":
            result = _indent(level) + "{\n"
        else:
            result = _indent(level) + repr(key) + " : {\n"
        for (k, v) in sorted(value.iteritems()):
            result += self.__raw_data(k, v, level+1)
        result += _indent(level) + "},\n"
        return result
    
    #################
    # Class methods #
    #################
    @classmethod
    def setToMetric(cls):
        cls.UrlUnits = cls.UnitsMetric

    @classmethod
    def isSetToMetric(cls):
        return cls.UrlUnits == cls.UnitsMetric

    @classmethod
    def setToImperial(cls):
        cls.UrlUnits = cls.UnitsImperial

    @classmethod
    def isSetToImperial(cls):
        return cls.UrlUnits == cls.UnitsImperial

    @classmethod
    def fromJson(cls, json_data):
        '''Instances a list of LocationInfo objects from a Json description.'''
        root = json.loads(json_data)

        if "list" not in root:
            return cls(root)
        return [cls(location) for location in root["list"]]

    #############
    # Operators #
    #############
    def __le__(self, other):
        if self.city != other.city: return self.city <= other.city
        if self.country != other.country: return self.country <= other.country
        return self.id <= other.id

    def __lt__(self, other):
        if self.city != other.city: return self.city < other.city
        if self.country != other.country: return self.country < other.country
        return self.id < other.id

    def __eq__(self, other):
        if self.city != other.city: return self.city == other.city
        if self.country != other.country: return self.country == other.country
        return self.id == other.id

    def __ne__(self, other):
        if self.city != other.city: return self.city != other.city
        if self.country != other.country: return self.country != other.country
        return self.id != other.id

    def __ge__(self, other):
        if self.city != other.city: return self.city >= other.city
        if self.country != other.country: return self.country >= other.country
        return self.id >= other.id

    def __gt__(self, other):
        if self.city != other.city: return self.city > other.city
        if self.country != other.country: return self.country > other.country
        return self.id > other.id

    ########################
    # Conversion to string #
    ########################
    def __str__(self):
        return "<LocationInfo id: {0}, name: {1}, country: {2}, latitude: {3}"\
            " longitude: {4}>".format(self.id, self.city, self.country,
            self.latitude, self.longitude)

    def __repr__(self):
        return "LocationInfo({0}, {1}, {2}, {3}, {4})".format(self.id,
            self.city, self.country, self.latitude, self.longitude)

