################################################################################
# Sweather - Shundread's Weather                                               #
# Copyright (C) 2013  Thiago Chaves de Oliveira Horta <shundread@gmail.com>    #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

# Default libs
import time # for debug
import os
import urllib2
import threading

# PySide
from PySide import QtCore
from PySide import QtGui

# Other project libs
from locationinfo import LocationInfo
from appkey import appkey

import datafetcher

class ListLocationModel(QtCore.QAbstractListModel):
    RoleId          = 100
    RoleCity        = 101
    RoleCountry     = 102
    RoleHumidity    = 103
    RoleLatitude    = 104
    RoleLongitude   = 105
    RolePressure    = 106
    RoleTemperature = 107
    RoleTempMax     = 108
    RoleTempMin     = 109
    RoleTempUnit    = 110
    RoleWeatherDesc = 111
    RoleWeatherIcon = 112
    RoleWeatherId   = 113
    RoleWeatherMain = 114
    RoleWindDegrees = 115
    RoleWindSpeed   = 116

    IconFolder = os.path.join(".", "icons")
    IconPath = os.path.join(IconFolder, "{0}.png")
    IconUrl = "http://openweathermap.org/img/w/{0}.png"

    def __init__(self):
        super(ListLocationModel, self).__init__()
        self.__icon_requests = []
        self.__locations = []

        # Locks
        self.__lock = threading.Lock()

    ##############
    # Properties #
    ##############
    @property
    def locations(self):
        with self.__lock:
            return self.__locations

    ####################
    # Element counting #
    ####################
    def rowCount(self, parent=QtCore.QModelIndex()):
        '''Returns the number of rows. Locks temporarily the access to the
        list's members.'''
        with self.__lock:
            return len(self.__locations)

    def columnCount(self, parent=QtCore.QModelIndex()):
        return 2

    ######################
    # Header information #
    ######################
    def headerData(self, section, orientation, role=QtCore.Qt.DisplayRole):
        if orientation != QtCore.Qt.Horizontal:
            return None

        if role == QtCore.Qt.DisplayRole: return "City"
        if role == ListLocationModel.RoleId: return "ID"
        if role == ListLocationModel.RoleCity: return "City"
        if role == ListLocationModel.RoleCountry: return "Country"
        if role == ListLocationModel.RoleLatitude: return "Latitude"
        if role == ListLocationModel.RoleLongitude: return "Longitude"
        if role == ListLocationModel.RolePressure: return "Pressure"
        if role == ListLocationModel.RoleTemperature: return "Temperature"
        if role == ListLocationModel.RoleTempMax: return "Maximum Temperature"
        if role == ListLocationModel.RoleTempMin: return "Minimum Temperature"
        if role == ListLocationModel.RoleTempUnit: return "Temperature Unit"
        if role == ListLocationModel.RoleWeatherDesc: return "Weather"
        if role == ListLocationModel.RoleWeatherIcon: return "Weather Icon"
        if role == ListLocationModel.RoleWeatherId: return "Weather ID"
        if role == ListLocationModel.RoleWeatherMain: return "Weather Main"
        if role == ListLocationModel.RoleWindDegrees: return "Wind Angle"
        if role == ListLocationModel.RoleWindSpeed: return "Wind Speed"

        return None

    #################
    # Data fetching #
    #################
    def data(self, index, role=QtCore.Qt.DisplayRole):
        '''Returns the data for a given row and a given role. Locks temporarily
        the access to the list's members.'''
        if not index.isValid():
            return None

        with self.__lock:
            location = self.__locations[index.row()]
            if role == QtCore.Qt.DisplayRole:
                return location.city
            if role == ListLocationModel.RoleId:
                return location.id
            if role == ListLocationModel.RoleCity:
                return location.city
            if role == ListLocationModel.RoleCountry:
                return location.country
            if role == ListLocationModel.RoleLatitude:
                return location.latitude
            if role == ListLocationModel.RoleLongitude:
                return location.longitude
            if role == ListLocationModel.RolePressure:
                return location.pressure
            if role == ListLocationModel.RoleTemperature:
                return location.temperature
            if role == ListLocationModel.RoleTempMax:
                return location.temperatureMax
            if role == ListLocationModel.RoleTempMin:
                return location.temperatureMin
            if role == ListLocationModel.RoleTempUnit:
                return location.temperatureUnit
            if role == ListLocationModel.RoleWeatherDesc:
                return location.weatherDesc
            if role == ListLocationModel.RoleWeatherIcon:
                return self.weatherIcon(location)
            if role == ListLocationModel.RoleWeatherId:
                return location.weatherId
            if role == ListLocationModel.RoleWeatherMain:
                return location.weatherMain
            if role == ListLocationModel.RoleWindDegrees:
                return location.windDegrees
            if role == ListLocationModel.RoleWindSpeed:
                return location.windSpeed

        return None

    #################
    # List updating #
    #################
    def appendData(self, newLocations):
        '''Adds rows to the list. Locks temporarily the access to the list's
        members.'''
        if type(newLocations) != list:
            newLocations = [newLocations]

        # Exclude locations that we already have
        with self.__lock:
            newLocations = [l for l in newLocations if l not in self.__locations]

        with self.__lock:
            last = len(self.__locations)
        self.beginInsertRows(QtCore.QModelIndex(), last, last + len(newLocations))
        with self.__lock:
            self.__locations.extend(newLocations)
        self.endInsertRows()

    def removeLocation(self, index):
        '''Removes a row from the list. Locks temporarily the access to the
        list's members.'''
        row = index.row()

        self.beginRemoveRows(QtCore.QModelIndex(), row, row)
        with self.__lock: self.__locations.pop(row)
        self.endRemoveRows()

    def replaceData(self, newLocations):
        '''Replaces the entire list. Locks temporarily the access to the list's
        members.'''
        if newLocations == None:
            newLocations = []

        self.beginResetModel()
        with self.__lock: self.__locations = newLocations
        self.endResetModel()

    def updateData(self, newLocations):
        '''Finds and updates locations that match the given set of locations.'''
        if type(newLocations) != list:
            newLocations = [newLocations]

        updated_rows = []
        with self.__lock:
            for new_location in newLocations:
                for row, old_location in enumerate(self.__locations):
                    if new_location.id == old_location.id:
                        self.__locations[row] = new_location
                        updated_rows.append(row)
                        continue

        for row in updated_rows:
            self.dataChanged.emit(self.index(row), self.index(row))

    def shiftRow(self, row, direction):
        if row+direction < 0 or row < 0:
            return
        with self.__lock:
            if row > len(self.__locations)\
            or row+direction > len(self.__locations):
                return
            location = self.__locations.pop(row)
            self.__locations.insert(row+direction, location)
        start = max(row, row+direction)
        end   = min(row, row+direction)
        self.dataChanged.emit(self.index(start), self.index(end))

    #######################
    # Specialized getters #
    #######################
    def weatherIcon(self, location):
        '''Returns the icon for a given location. This function may return None
        for LocationInfo objects that do not define a weather icon or don't have
        a local copy of the icon. In the second case, this function will make
        a request from the online service for the icon and emit an update when
        it arrives.'''
        # Check if we have an icon name.
        icon_name = location.weatherIcon
        if not icon_name:
            return None

        # Make sure that we have an icon folder
        if not os.path.exists(ListLocationModel.IconFolder):
            os.mkdir(ListLocationModel.IconFolder)
            
        # Check if we already have the icon in that folder
        icon_path = ListLocationModel.IconPath.format(icon_name)
        icon_info = QtCore.QFileInfo(icon_path)

        # If not, post the request and temporarily return none
        if not icon_info.exists():
            # Make sure each icon is requested only once.
            if icon_name not in self.__icon_requests:
                self.__icon_requests.append(icon_name)
                icon_url = ListLocationModel.IconUrl.format(icon_name)
                datafetcher.listener.request(icon_url, self.handleIconData, icon_path)
            return None

        # If so, return the proper QPixMap()
        return QtGui.QPixmap(icon_path)

    #################
    # Data fetching #
    #################
    def grabSearchResults(self, callback, city, country=None):
        '''Grabs a list of LocationInfo objects according to the given query.'''
        # Calculate the proper URL
        if country and len(country) > 0:
            location = "{0},{1}".format(city, country)
        else:
            location = city
        url = LocationInfo.UrlSearch.format(location) + LocationInfo.UrlUnits

        # Request data
        datafetcher.listener.request(url, callback, key=appkey)

    def grabFromId(self, callback, location_id):
        '''Grabs a LocationInfo object from its ID.'''
        # Calculate the proper URL
        url = LocationInfo.UrlFromId.format(location_id) + LocationInfo.UrlUnits

        # Request data
        datafetcher.listener.request(url, callback, key=appkey)

    ######################
    # Net data callbacks #
    ######################
    def appendDataWithJsonData(self, json_data):
        '''Appends new rows to the list with data from a JSON string. Blocks
        temporarily access to the list's current members.'''
        locations = LocationInfo.fromJson(json_data)
        self.appendData(locations)

    def replaceDataWithJsonData(self, json_data):
        '''Replaces the list's population with data from a JSON string. Blocks
        temporarily access to the list's current members.'''
        locations = LocationInfo.fromJson(json_data)
        self.replaceData(locations)

    def updateDataWithJsonData(self, json_data):
        '''Finds and update the appropriate row with data from a JSON string.
        Blocks temporarily access to the list's current members.'''
        locations = LocationInfo.fromJson(json_data)
        self.updateData(locations)

    def handleIconData(self, icon_data, icon_path):
        '''Updates every row of the model which was waiting to have a particular
        icon.'''
        # Write the icon data onto a file.
        icon_file = open(icon_path, "wb")
        icon_file.write(icon_data)
        icon_file.close()

        # Emit a model reset so all rows that reference the same icon get an
        # update.
        self.beginResetModel()
        self.endResetModel()

    #########
    # Slots #
    #########
    def refreshAll(self):
        print("Updating everything at {0}".format(time.ctime()))
        for location in self.__locations:
            url = LocationInfo.UrlFromId.format(location.id) + LocationInfo.UrlUnits
            datafetcher.listener.request(url, self.updateDataWithJsonData)
