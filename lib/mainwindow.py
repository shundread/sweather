################################################################################
# Sweather - Shundread's Weather                                               #
# Copyright (C) 2013  Thiago Chaves de Oliveira Horta <shundread@gmail.com>    #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

# Qt imports
from PySide import QtCore
from PySide import QtGui

# UI import
from forms.mainwindow import Ui_MainWindow as UiMainWindow

# Other dialogs
from addlocationdialog import AddLocationDialog
from aboutdialog import AboutDialog

# Lib
from listlocationmodel import ListLocationModel
from locationinfo import LocationInfo

######################
# List item delegate #
######################
class ItemLocationDelegate(QtGui.QAbstractItemDelegate):
    Margin = 10

    indexSelected = QtCore.Signal(QtCore.QModelIndex)
    rowSelected = QtCore.Signal(int)

    def __init__(self, parent=None):
        super(ItemLocationDelegate, self).__init__(parent)
        self.__index = QtCore.QModelIndex()

    ##############
    # Properties #
    ##############
    @property
    def index(self):
        return self.__index
    @index.setter
    def index(self, newValue):
        if self.__index != newValue:
            self.__index = newValue
            self.indexSelected.emit(self.__index)
            self.rowSelected.emit(self.__index.row())

    #########################
    # Calculating size hint #
    #########################
    def sizeHint(self, option, index):
        return QtCore.QSize(
            option.rect.width(),
            option.fontMetrics.height()*3 + 2*ItemLocationDelegate.Margin
        )

    ####################
    # Drawing the item #
    ####################
    def paint(self, painter, option, index):
        if not index.isValid():
            return

        margin = ItemLocationDelegate.Margin
        colSize = int((option.rect.width() - 3*margin) / 3.0)
        rowSize = option.fontMetrics.height()
        defaultFont = painter.font()

        # Setup other fonts
        cFont = QtGui.QFont(defaultFont)
        cFont.setBold(True)
        cFont.setPointSize(12)
        cFontM = QtGui.QFontMetrics(cFont)

        dFont = QtGui.QFont(defaultFont)
        dFont.setPointSize(9)
        dFontM = QtGui.QFontMetrics(dFont)

        tFont = QtGui.QFont(defaultFont)
        tFont.setBold(True)
        tFont.setPointSize(14)
        tFontM = QtGui.QFontMetrics(tFont)

        tmmFont = QtGui.QFont(defaultFont)
        tmmFont.setBold(True)
        tmmFont.setPointSize(10)
        tmmFontM = QtGui.QFontMetrics(tmmFont)
        tmmWidth = tmmFontM.width(u' 00.0\xb0C ')


        ##############################
        # Choosing the right pallete #
        ##############################
        if index.row() == self.index.row():
            painter.save()
            painter.setBrush(option.palette.highlightedText())
            painter.fillRect(option.rect, option.palette.highlight())
            painter.setPen(painter.brush().color())
        else:
            painter.save()
            painter.setBrush(option.palette.text())
            painter.setPen(painter.brush().color())

        ############################
        # Drawing the weather icon #
        ############################
        icon = index.data(ListLocationModel.RoleWeatherIcon)
        if(icon):
            ystart = (option.rect.height() - icon.height()) / 2
            icon_rect = QtCore.QRect(
                option.rect.x() + margin,
                option.rect.y() + ystart,
                icon.width(),
                icon.height()
            )

            painter.fillRect(icon_rect, QtCore.Qt.lightGray)
            painter.drawPixmap(icon_rect, icon)
            cityWShift = 2*margin + icon.width()
        else:
            cityWShift = margin

        #########################
        # Drawing the city name #
        #########################
        city = index.data(ListLocationModel.RoleCity)
        city_rect = QtCore.QRect(
            option.rect.x() + cityWShift,
            option.rect.y() + margin,
            cFontM.width(city),
            cFontM.height()
        )

        painter.save()
        painter.setFont(cFont)
        painter.drawText(
            city_rect,
            QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter,
            option.fontMetrics.elidedText(city, QtCore.Qt.ElideRight, colSize)
        )
        painter.restore()

        ###################################
        # Drawing the weather description #
        ###################################
        wdesc = index.data(ListLocationModel.RoleWeatherDesc)
        wdesc_rect = QtCore.QRect(
            option.rect.x() + cityWShift,
            option.rect.y() + margin*2 + cFontM.height(),
            dFontM.width(wdesc),
            dFontM.height()
        )

        painter.drawText(
            wdesc_rect,
            QtCore.Qt.AlignLeft | QtCore.Qt.AlignBottom,
            wdesc
        )

        ###########################
        # Drawing the temperature #
        ###########################
        temp = u'{0:.1f}'.format(index.data(ListLocationModel.RoleTemperature)) +\
               index.data(ListLocationModel.RoleTempUnit)
        temp_rect = QtCore.QRect(
            option.rect.x(),
            option.rect.y(),
            option.rect.width() - margin - tmmWidth,
            option.rect.height()
        )

        painter.save()
        painter.setFont(tFont)
        painter.drawText(
            temp_rect,
            QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter,
            temp
        )
        painter.restore()

        ###############################################
        # Drawing the minimum and maximum temperature #
        ###############################################
        painter.save()
        painter.setFont(tmmFont)

        tempMax = u'{0:.1f}'.format(index.data(ListLocationModel.RoleTempMax)) +\
                  index.data(ListLocationModel.RoleTempUnit)
        tempMax_rect = QtCore.QRect(
            option.rect.x(),
            option.rect.y() + margin,
            option.rect.width() - margin,
            option.rect.height()
        )

        painter.setPen(QtCore.Qt.darkRed)
        painter.drawText(
            tempMax_rect,
            QtCore.Qt.AlignRight | QtCore.Qt.AlignTop,
            tempMax
        )

        tempMin = u'{0:.1f}'.format(index.data(ListLocationModel.RoleTempMin)) +\
                  index.data(ListLocationModel.RoleTempUnit)
        tempMin_rect = QtCore.QRect(
            option.rect.x(),
            option.rect.y() - margin,
            option.rect.width() - margin,
            option.rect.height()
        )

        painter.setPen(QtCore.Qt.darkBlue)
        painter.drawText(
            tempMin_rect,
            QtCore.Qt.AlignRight | QtCore.Qt.AlignBottom,
            tempMin
        )
        painter.restore()

        #########################
        # Restoring the palette #
        #########################
        painter.restore()

        #######################
        # Drawing a delimiter #
        #######################
        if index.row() == 0:
            return
        painter.save()
        painter.setPen(QtCore.Qt.gray)
        painter.drawLine(
            option.rect.x(),
            option.rect.y(),
            option.rect.x() + option.rect.width(),
            option.rect.y()
        )
        painter.restore()

    ##################
    # Handling input #
    ##################
    def editorEvent(self, event, model, option, index):
        if event.type() == QtCore.QEvent.MouseButtonPress:
            self.index = index
        return True

    def clearSelection(self):
        self.index = QtCore.QModelIndex()


###############
# Main window #
###############
class MainWindow(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.ui = UiMainWindow()
        self.ui.setupUi(self)

        ############################
        # Setting up the list view #
        ############################
        self.listLocation = ListLocationModel()
        self.itemLocationDelegate = ItemLocationDelegate(self)
        self.ui.viewLocation.setModel(self.listLocation)
        self.ui.viewLocation.setItemDelegate(self.itemLocationDelegate)

        ######################
        # Connecting signals #
        ######################
        # File menu
        self.ui.actionExit.triggered.connect(self.onExit)
        self.ui.actionAddLocation.triggered.connect(self.addLocation)
        self.ui.actionRemoveLocation.triggered.connect(self.removeLocation)

        # View menu
        self.ui.actionMetric.triggered.connect(self.useMetricSystem)
        self.ui.actionImperial.triggered.connect(self.useImperialSystem)

        # About menu
        self.ui.actionAbout.triggered.connect(self.about)

        # Weather view
        self.itemLocationDelegate.indexSelected.connect(self.ui.viewLocation.setCurrentIndex)
        self.itemLocationDelegate.rowSelected.connect(self.locationIndexSelected)

        #########################
        # Setting the tray icon #
        #########################
        if QtGui.QSystemTrayIcon.isSystemTrayAvailable():
            self.trayIcon = QtGui.QSystemTrayIcon(self)
            self.trayIcon.setIcon(self.windowIcon())
            self.trayIcon.activated.connect(self.trayIconActivated)
            self.trayIcon.show()
        else:
            self.trayIcon = None

        ###############################
        # Setting up a sensible timer #
        ###############################
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.listLocation.refreshAll)
        self.timer.start(LocationInfo.ExpirationTime)
        
    #########
    # Slots #
    #########
    def addLocation(self):
        dialog = AddLocationDialog(self)
        result = dialog.exec_()
        if result > 0:
            self.listLocation.appendData(dialog.currentLocation)

    def removeLocation(self):
        self.listLocation.removeLocation(self.itemLocationDelegate.index)
        self.itemLocationDelegate.clearSelection()
        self.ui.actionRemoveLocation.setEnabled(False)

    def about(self):
        dialog = AboutDialog(self)
        dialog.exec_()

    def closeEvent(self, event):
        if self.trayIcon:
            print("Minimizing to tray")
        else:
            self.onExit()

    def onExit(self):
        self.saveSettings()
        QtCore.QCoreApplication.exit()

    ################
    # View actions #
    ################
    def useMetricSystem(self):
        # Stop the timer
        self.timer.stop()

        # Update the actions' checked status
        self.ui.actionMetric.setChecked(True)
        self.ui.actionImperial.setChecked(False)

        # Set LocationInfo units to Metric
        LocationInfo.setToMetric()

        # Refresh everything and restart the timer
        self.listLocation.refreshAll()
        self.timer.start(LocationInfo.ExpirationTime)

    def useImperialSystem(self):
        # Stop the timer
        self.timer.stop()

        # Update the actions' checked status
        self.ui.actionMetric.setChecked(False)
        self.ui.actionImperial.setChecked(True)

        # Set LocationInfo units to Imperial
        LocationInfo.setToImperial()

        # Refresh everything and restart the timer
        self.listLocation.refreshAll()
        self.timer.start(LocationInfo.ExpirationTime)

    ########################
    # Other event handlers #
    ########################
    def locationIndexSelected(self, index):
        if index == -1:
            self.ui.actionRemoveLocation.setEnabled(False)
            self.ui.actionMoveUp.setEnabled(False)
            self.ui.actionMoveDown.setEnabled(False)
        else:
            nrows = self.listLocation.rowCount()
            self.ui.actionRemoveLocation.setEnabled(True)
            self.ui.actionMoveUp.setEnabled(index > 0)
            self.ui.actionMoveDown.setEnabled(index < nrows - 1)
        self.ui.viewLocation.update()

    def shiftRowUp(self):
        row = self.itemLocationDelegate.index.row()
        if row >= 0:
            self.listLocation.shiftRow(row, -1)
            new_index = self.listLocation.index(row-1)
            self.itemLocationDelegate.index = new_index

    def shiftRowDown(self):
        row = self.itemLocationDelegate.index.row()
        if row >= 0:
            self.listLocation.shiftRow(row, +1)
            new_index = self.listLocation.index(row+1)
            self.itemLocationDelegate.index = new_index

    def trayIconActivated(self):
        if self.isVisible():
            self.hide()
        else:
            self.show()

    ############
    # Settings #
    ############
    def loadSettings(self):
        print("Loading settings")
        settings = QtCore.QSettings()

        ##############################
        # Load preferred unit system #
        ##############################
        if settings.contains("units"):
            units = settings.value("units")
        else:
            units = "metric"

        if units == "metric":
            self.useMetricSystem()
        else:
            self.useImperialSystem()

        ##################
        # Load locations #
        ##################
        if settings.contains("locations"):
            ids = settings.value("locations")
        else:
            ids = []
        if not ids: # Sometimes settings.value("locations") returns None
            ids = []

        print("Restoring ids: {0}".format(ids))
        for l_id in ids:
            self.listLocation.grabFromId(
                self.listLocation.appendDataWithJsonData,
                l_id
            )

    def saveSettings(self):
        settings = QtCore.QSettings()

        ###############################
        # Store preferred unit system #
        ###############################
        if LocationInfo.isSetToMetric():
            settings.setValue("units", "metric")
        else:
            settings.setValue("units", "imperial")
        print("Preferred unit system: {0}".format(settings.value("units")))

        ###################
        # Store locations #
        ###################
        ids = [location.id for location in self.listLocation.locations]
        print("Storing ids: {0}".format(ids))
        settings.setValue("locations", ids)

