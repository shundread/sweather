# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'addlocation.ui'
#
# Created: Wed Oct  9 08:40:08 2013
#      by: pyside-uic 0.2.13 running on PySide 1.1.1
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(473, 455)
        Dialog.setLocale(QtCore.QLocale(QtCore.QLocale.English, QtCore.QLocale.UnitedStates))
        Dialog.setModal(True)
        self.gridLayout = QtGui.QGridLayout(Dialog)
        self.gridLayout.setObjectName("gridLayout")
        self.labelCountry = QtGui.QLabel(Dialog)
        self.labelCountry.setObjectName("labelCountry")
        self.gridLayout.addWidget(self.labelCountry, 1, 0, 1, 1)
        self.labelCity = QtGui.QLabel(Dialog)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Maximum, QtGui.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.labelCity.sizePolicy().hasHeightForWidth())
        self.labelCity.setSizePolicy(sizePolicy)
        self.labelCity.setObjectName("labelCity")
        self.gridLayout.addWidget(self.labelCity, 0, 0, 1, 1)
        self.refreshButton = QtGui.QPushButton(Dialog)
        self.refreshButton.setEnabled(False)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.refreshButton.sizePolicy().hasHeightForWidth())
        self.refreshButton.setSizePolicy(sizePolicy)
        self.refreshButton.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.refreshButton.setStyleSheet("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icons/system-search"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.refreshButton.setIcon(icon)
        self.refreshButton.setFlat(False)
        self.refreshButton.setObjectName("refreshButton")
        self.gridLayout.addWidget(self.refreshButton, 2, 2, 1, 1)
        self.buttonBox = QtGui.QDialogButtonBox(Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayout.addWidget(self.buttonBox, 4, 0, 1, 3)
        self.editCity = QtGui.QLineEdit(Dialog)
        self.editCity.setObjectName("editCity")
        self.gridLayout.addWidget(self.editCity, 0, 1, 1, 2)
        self.editCountry = QtGui.QLineEdit(Dialog)
        self.editCountry.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.editCountry.setObjectName("editCountry")
        self.gridLayout.addWidget(self.editCountry, 1, 1, 1, 2)
        self.viewLocation = QtGui.QListView(Dialog)
        self.viewLocation.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.viewLocation.setObjectName("viewLocation")
        self.gridLayout.addWidget(self.viewLocation, 3, 0, 1, 3)
        self.actionOnRefresh = QtGui.QAction(Dialog)
        self.actionOnRefresh.setObjectName("actionOnRefresh")
        self.labelCountry.setBuddy(self.editCountry)
        self.labelCity.setBuddy(self.editCity)

        self.retranslateUi(Dialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("accepted()"), Dialog.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("rejected()"), Dialog.reject)
        QtCore.QObject.connect(self.refreshButton, QtCore.SIGNAL("clicked()"), self.actionOnRefresh.trigger)
        QtCore.QObject.connect(self.editCity, QtCore.SIGNAL("textEdited(QString)"), Dialog.onCityEdit)
        QtCore.QObject.connect(self.viewLocation, QtCore.SIGNAL("clicked(QModelIndex)"), Dialog.onLocationSelected)
        QtCore.QObject.connect(self.actionOnRefresh, QtCore.SIGNAL("triggered()"), Dialog.onRefresh)
        QtCore.QObject.connect(self.editCity, QtCore.SIGNAL("returnPressed()"), self.actionOnRefresh.trigger)
        QtCore.QObject.connect(self.editCountry, QtCore.SIGNAL("returnPressed()"), self.actionOnRefresh.trigger)
        QtCore.QMetaObject.connectSlotsByName(Dialog)
        Dialog.setTabOrder(self.editCity, self.editCountry)
        Dialog.setTabOrder(self.editCountry, self.refreshButton)
        Dialog.setTabOrder(self.refreshButton, self.viewLocation)
        Dialog.setTabOrder(self.viewLocation, self.buttonBox)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtGui.QApplication.translate("Dialog", "Dialog", None, QtGui.QApplication.UnicodeUTF8))
        self.labelCountry.setText(QtGui.QApplication.translate("Dialog", "Cou&ntry:", None, QtGui.QApplication.UnicodeUTF8))
        self.labelCity.setText(QtGui.QApplication.translate("Dialog", "&City:", None, QtGui.QApplication.UnicodeUTF8))
        self.refreshButton.setText(QtGui.QApplication.translate("Dialog", "&Search cities", None, QtGui.QApplication.UnicodeUTF8))
        self.editCity.setToolTip(QtGui.QApplication.translate("Dialog", "Write down the city and country names for this location", None, QtGui.QApplication.UnicodeUTF8))
        self.editCity.setPlaceholderText(QtGui.QApplication.translate("Dialog", "name of the city", None, QtGui.QApplication.UnicodeUTF8))
        self.editCountry.setPlaceholderText(QtGui.QApplication.translate("Dialog", "name of the country", None, QtGui.QApplication.UnicodeUTF8))
        self.actionOnRefresh.setText(QtGui.QApplication.translate("Dialog", "Refreshes the query list", None, QtGui.QApplication.UnicodeUTF8))

import resources_rc
